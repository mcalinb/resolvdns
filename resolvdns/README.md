# resovedns
An ansible playbook to configure /etc/resolv.conf
#
This is actually a wrapper for the ahuffman.resolv galaxy role

To run it use this as an example:
#
ansible-playbook -i ../../hosts.lab ../../hosts.dev main.yml -kK --check
